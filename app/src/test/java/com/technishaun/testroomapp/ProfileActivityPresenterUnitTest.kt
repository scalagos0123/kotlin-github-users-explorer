package com.technishaun.testroomapp

import com.nhaarman.mockitokotlin2.*
import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.objects.response.ResponseGithubUser
import com.technishaun.testroomapp.objects.response.ResponseGithubUsers
import com.technishaun.testroomapp.objects.response.ResponseGithubUsersItem
import com.technishaun.testroomapp.repository.local.LocalRepositoryImpl
import com.technishaun.testroomapp.repository.remote.RemoteRepositoryImpl
import com.technishaun.testroomapp.screens.profile.ProfileActivityContract
import com.technishaun.testroomapp.screens.profile.ProfileActivityPresenter
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test

class ProfileActivityPresenterUnitTest {
    private val mainThread = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(mainThread)
    private val githubUsers = ResponseGithubUsers().apply {
        add(ResponseGithubUsersItem("a", 1, "abclogin"))
        add(ResponseGithubUsersItem("b", 2, "deflogin"))
    }

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(mainThread)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun test_checkCompleteCachedData() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>(
            User(
                username = "abclogin",
                imageUrl = "a",
                name = "Juan Dela Cruz",
                followers = 20,
                following = 19,
                companyName = "company",
                blog = "blog"
            ).apply { id = 1 }
        )

        val localRepository = mock<LocalRepositoryImpl> {
            on { findUser(any() as Int) }.thenReturn(table[0])
        }

        val remoteRepository = mock<RemoteRepositoryImpl> {
            onBlocking { getUserInfo(any()) }.then { throw Exception() }
        }

        val view = mock<ProfileActivityContract.View> {
        }

        val job = Job()

        // start test
        val presenter = ProfileActivityPresenter(
            remoteRepository,
            localRepository,
            job,
            subscribeScheduler = mainThread
        )

        presenter.attachView(view)

        presenter.getUser(1)

        launch {
            verify(view, times(1)).hideLoadingScreen()
            verify(view, times(1)).showUserDetails(any())
            verify(view, times(0)).showLoadingScreen()
            verify(view, times(0)).showRetryButton()
        }
    }

    /**
     * Test should update saved user in db
     * and update user in db after fetching data
     */
    @ExperimentalCoroutinesApi
    @Test
    fun test_checkIncompleteData() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>(
            User(
                username = "abclogin",
                imageUrl = "a"
            ).apply { id = 1 }
        )

        val localRepository = mock<LocalRepositoryImpl> {
            on { findUser(any() as Int) }.thenReturn(table[0])
            on { updateUser(any()) }.then {
                val user = it.arguments[0] as User
                if (user.id == table[0].id) {
                    table[0] = user
                }
            }
        }

        val remoteRepository = mock<RemoteRepositoryImpl> {
            onBlocking { getUserInfo(any()) }.thenReturn(ResponseGithubUser(
                avatarUrl = "a",
                blog = "blog",
                company = "company",
                followers = 2,
                following = 1,
                id = 1,
                login = "abclogin",
                name = "abc name"
            ))
        }

        val view = mock<ProfileActivityContract.View> {

        }

        val job = Job()

        // start test
        val presenter = ProfileActivityPresenter(
            remoteRepository,
            localRepository,
            job,
            subscribeScheduler = mainThread
        )

        presenter.attachView(view)

        presenter.getUser(1)

        launch {
            verify(view, times(1)).hideLoadingScreen()
            verify(view, times(1)).showUserDetails(any())
            verify(view, times(0)).showLoadingScreen()
            verify(view, times(0)).showRetryButton()
            val userInTable = table[0]

            // only asserting important data to be shown
            assertTrue(
                userInTable.name != null
                && userInTable.followers > -1
                && userInTable.following > -1
            )
        }
    }

    /**
     * Test should update saved user in db
     * and update user in db after fetching data
     */
    @Test
    fun test_updateNote() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>(
            User(
                username = "abclogin",
                imageUrl = "a"
            ).apply { id = 1 }
        )

        val localRepository = mock<LocalRepositoryImpl> {
            on { findUser(any() as Int) }.thenReturn(table[0])
            on { updateUser(any()) }.then {
                val user = it.arguments[0] as User
                if (user.id == table[0].id) {
                    table[0] = user
                }
            }
        }

        val remoteRepository = mock<RemoteRepositoryImpl> {
            onBlocking { getUserInfo(any()) }.thenReturn(ResponseGithubUser(
                avatarUrl = "a",
                blog = "blog",
                company = "company",
                followers = 2,
                following = 1,
                id = 1,
                login = "abclogin",
                name = "abc name"
            ))
        }

        val view = mock<ProfileActivityContract.View> {

        }

        val job = Job()

        // start test
        val presenter = ProfileActivityPresenter(
            remoteRepository,
            localRepository,
            job,
            subscribeScheduler = mainThread
        )

        presenter.attachView(view)

        presenter.getUser(1)
        presenter.saveNote("note")

        launch {
            verify(view, times(1)).hideLoadingScreen()
            verify(view, times(1)).showUserDetails(any())
            verify(view, times(0)).showLoadingScreen()
            verify(view, times(0)).showRetryButton()
            verify(view, times(1)).markNoteAsSaved(any())
            verify(view, times(1)).showToastMessage(any())

            val userInTable = table[0]
            assertTrue(userInTable.note == "note")
        }
    }
}