package com.technishaun.testroomapp

import com.nhaarman.mockitokotlin2.*
import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.objects.response.ResponseGithubUsers
import com.technishaun.testroomapp.objects.response.ResponseGithubUsersItem
import com.technishaun.testroomapp.repository.local.LocalRepositoryImpl
import com.technishaun.testroomapp.repository.remote.RemoteRepositoryImpl
import com.technishaun.testroomapp.screens.main.MainActivityContract
import com.technishaun.testroomapp.screens.main.MainActivityPresenter
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class MainActivityPresenterUnitTest {

    private val mainThread = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(mainThread)
    private val githubUsers = ResponseGithubUsers().apply {
        add(ResponseGithubUsersItem("a", 1, "abclogin"))
        add(ResponseGithubUsersItem("b", 2, "deflogin"))
    }

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(mainThread)
    }

    /**
     * Tests if data has no duplicates on view and database
     */
    @ExperimentalCoroutinesApi
    @Test
    fun test_getUsersNonDuplicateOnRemoteCall() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>()

        // creating another table for view-side duplication check
        val displayedItems = mutableListOf<User>()

        val parentJob = Job()
        val remoteRepositoryImpl = mock<RemoteRepositoryImpl> {
            onBlocking { getUsers(any()) }.thenReturn(githubUsers)
        }

        val localRepositoryImpl = mock<LocalRepositoryImpl> {

            // mocking cached users
            on { getUsers() }.thenReturn(table)
            on { insertUsers(anyVararg()) }.then {
                // store info
                it.arguments.forEach { currentUser ->
                    val user = currentUser as User
                    table.add(user)
                }
            }
        }

        // mocking view for verification
        val view: MainActivityContract.View = mock { mock ->
            on { displayUsers(any(), any(), any()) }.then {
                displayedItems.addAll(it.arguments[0] as List<User>)
            }
        }

        // starting test
        val presenter = MainActivityPresenter(
            remoteRepository = remoteRepositoryImpl,
            subscribeScheduler = mainThread,
            parentJob = parentJob,
            localRepository = localRepositoryImpl
        )

        presenter.attachView(view)

        presenter.getUsers(-1)
        presenter.getUsers(2)

        launch {
            // verify table and displayItems
            verify(view, times(3)).displayUsers(any(), any(), any())
            assertTrue(table == displayedItems)
        }
    }

    /**
     * Tests if data has no duplicates on view and database on app relaunch
     */
    @ExperimentalCoroutinesApi
    @Test
    fun test_getUsersNonDuplicateOnAppStart() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>()

        // creating another table for view-side duplication check
        val displayedItems = mutableListOf<User>()

        val parentJob = Job()
        val remoteRepositoryImpl = mock<RemoteRepositoryImpl> {
            onBlocking { getUsers(any()) }.thenReturn(githubUsers)
        }

        val localRepositoryImpl = mock<LocalRepositoryImpl> {

            // mocking cached users
            on { getUsers() }.thenReturn(table)
            on { insertUsers(anyVararg()) }.then {
                // store info
                it.arguments.forEach { currentUser ->
                    val user = currentUser as User
                    table.add(user)
                }
            }
        }

        // mocking view for verification
        val view: MainActivityContract.View = mock { mock ->
            on { displayUsers(any(), any(), any()) }.then {
                displayedItems.addAll(it.arguments[0] as List<User>)
            }
        }

        // starting test
        val presenter = MainActivityPresenter(
            remoteRepository = remoteRepositoryImpl,
            subscribeScheduler = mainThread,
            parentJob = parentJob,
            localRepository = localRepositoryImpl
        )

        presenter.attachView(view)

        presenter.getUsers(-1)

        // next app launch
        displayedItems.clear()
        presenter.getUsers(-1)

        launch {
            // verify table and displayItems
            verify(view, times(4)).displayUsers(any(), any(), any())
            assertTrue(table.size == 2 && displayedItems.size == 2)
        }
    }

    @Test
    fun test_searchUser() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>(
            User("abclogin", "a", note = "note1").apply { id = 1 },
            User("deflogin", "b", note = "note2").apply { id = 2 }
        )

        // creating another table for view-side duplication check
        val displayedItems = mutableListOf<User>()

        val parentJob = Job()
        val remoteRepositoryImpl = mock<RemoteRepositoryImpl> {
            onBlocking { getUsers(any()) }.thenReturn(githubUsers)
        }

        val localRepositoryImpl = mock<LocalRepositoryImpl> {

            // mocking cached users
            on { getUsers() }.thenReturn(table)
        }

        // mocking view for verification
        val view: MainActivityContract.View = mock { mock ->
            on { displayUsers(any(), any(), any()) }.then {
                displayedItems.clear()
                displayedItems.addAll(it.arguments[0] as List<User>)
            }
        }

        // starting test
        val presenter = MainActivityPresenter(
            remoteRepository = remoteRepositoryImpl,
            subscribeScheduler = mainThread,
            parentJob = parentJob,
            localRepository = localRepositoryImpl
        )

        presenter.attachView(view)

        presenter.getUsers(-1)

        // performing user search
        presenter.searchUser("login")
        launch {
            verify(view, times(3)).displayUsers(any(), any(), any())
            assertTrue(displayedItems.size == 2
                    && displayedItems.find { it.username == "abclogin" } != null
                    && displayedItems.find { it.username == "deflogin" } != null
            )
        }.join()
    }

    @Test
    fun test_searchNote() = testScope.runBlockingTest {
        // creating a mock table for duplication check
        val table = mutableListOf<User>(
            User("abclogin", "a", note = "note1").apply { id = 1 },
            User("deflogin", "b", note = "note2").apply { id = 2 }
        )

        // creating another table for view-side duplication check
        val displayedItems = mutableListOf<User>()

        val parentJob = Job()
        val remoteRepositoryImpl = mock<RemoteRepositoryImpl> {
            onBlocking { getUsers(any()) }.thenReturn(ResponseGithubUsers())
        }

        val localRepositoryImpl = mock<LocalRepositoryImpl> {

            // mocking cached users
            on { getUsers() }.thenReturn(table)
        }

        // mocking view for verification
        val view: MainActivityContract.View = mock { mock ->
            on { displayUsers(any(), any(), any()) }.then {
                displayedItems.clear()
                displayedItems.addAll(it.arguments[0] as List<User>)
            }
        }

        // starting test
        val presenter = MainActivityPresenter(
            remoteRepository = remoteRepositoryImpl,
            subscribeScheduler = mainThread,
            parentJob = parentJob,
            localRepository = localRepositoryImpl
        )

        presenter.attachView(view)

        presenter.getUsers(-1)

        // searching for note
        presenter.searchUser("note1")
        launch {
            verify(view, times(3)).displayUsers(any(), any(), any())
            assertTrue(displayedItems.size == 1
                    && displayedItems.find { it.note == "note1" } != null
            )
        }.join()
    }
}