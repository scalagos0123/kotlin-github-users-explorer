package com.technishaun.testroomapp.objects.dbinstance

import androidx.room.Database
import androidx.room.RoomDatabase
import com.technishaun.testroomapp.objects.daos.UserDao
import com.technishaun.testroomapp.objects.db.User

@Database(entities = [User::class], version = 2)
abstract class AppDbInstance: RoomDatabase() {
    abstract fun userDao(): UserDao
}