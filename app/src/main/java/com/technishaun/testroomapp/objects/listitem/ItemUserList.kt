package com.technishaun.testroomapp.objects.listitem

import android.graphics.ColorMatrixColorFilter
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.technishaun.testroomapp.R
import com.technishaun.testroomapp.objects.db.User
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class ItemUserList(
    val user: User,
    val invertedColor: Boolean
): Item<GroupieViewHolder>() {

    var listener: ((ItemUserList) -> Unit)? = null
    private var mAvatar: ImageView? = null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            findViewById<TextView>(R.id.txtv_user_name).text = user.username
            findViewById<TextView>(R.id.txtv_user_details).text = "User ID: ${user.id}"
            findViewById<ImageView>(R.id.imgv_note).visibility = if (user.note.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE

            mAvatar = findViewById(R.id.imgv_avatar)

            if (invertedColor) {
                // setting inverted color filter
                // solution from https://stackoverflow.com/questions/17841787/invert-colors-of-drawable
                val negative = floatArrayOf(
                    -1.0f,     .0f,     .0f,    .0f,  255.0f,
                    .0f,   -1.0f,     .0f,    .0f,  255.0f,
                    .0f,     .0f,   -1.0f,    .0f,  255.0f,
                    .0f,     .0f,     .0f,   1.0f,     .0f
                )

                mAvatar!!.colorFilter = ColorMatrixColorFilter(negative)
            }

            // caching data to disk
            Glide.with(this)
                .load(user.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(mAvatar!!)

            setOnClickListener {
                listener?.invoke(this@ItemUserList)
            }
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)

        // freeing up image to prevent memory leaks
        mAvatar?.let {
            if (invertedColor) it.clearColorFilter()
            Glide.with(viewHolder.itemView).clear(it)
        }

        mAvatar = null
    }

    override fun getLayout(): Int = R.layout.row_user_list
}