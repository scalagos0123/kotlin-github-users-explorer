package com.technishaun.testroomapp.objects.daos

import androidx.room.*
import com.technishaun.testroomapp.objects.db.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user ORDER BY user.id ASC")
    fun getAll(): List<User>

    @Insert
    fun insertUsers(vararg users: User)

    @Delete
    fun deleteUser(user: User)

    @Query("SELECT * FROM user WHERE name LIKE :name || '%' LIMIT 1")
    fun getUserByName(name: String): User

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUserById(id: Int): User

    @Update
    fun updateUser(user: User)

    @Query("SELECT MAX(id) from user")
    fun getLastInsertedUserId(): Int
}