package com.technishaun.testroomapp.objects.base

interface BasePresenter<T> {
    fun attachView(view: T)
}