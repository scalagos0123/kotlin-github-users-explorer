package com.technishaun.testroomapp.objects.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @ColumnInfo(name = "username") var username: String,
    @ColumnInfo(name = "imageUrl") var imageUrl: String,
    @ColumnInfo(name = "name") var name: String? = null,
    @ColumnInfo(name = "note") var note: String? = null,
    @ColumnInfo(name = "company") var companyName: String? = null,
    @ColumnInfo(name = "followers") var followers: Int = -1,
    @ColumnInfo(name = "following") var following: Int = -1,
    @ColumnInfo(name = "blog") var blog: String? = null
) {
    @PrimaryKey
    var id: Int = 0
}