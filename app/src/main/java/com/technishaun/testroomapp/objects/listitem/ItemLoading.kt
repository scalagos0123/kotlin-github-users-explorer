package com.technishaun.testroomapp.objects.listitem

import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import com.technishaun.testroomapp.R
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class ItemLoading(
        val nextUserId: Int,
        val initialInvokeOnBind: Boolean = true
): Item<GroupieViewHolder>() {

    var listener: ((ItemLoading) -> Unit)? = null

    private var mProgressBar: ProgressBar? = null
    private var mRetryButton: Button? = null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        // fixing progressbar freeze issue
        mProgressBar = viewHolder.itemView.findViewById(R.id.prg_loading)
        mRetryButton = viewHolder.itemView.findViewById(R.id.btn_retry)

        mProgressBar?.let { it.visibility = View.VISIBLE }
        mRetryButton?.setOnClickListener(mRetryListener)

        // call listener on bind
       if (initialInvokeOnBind) listener?.invoke(this)
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)

        // fixing progressbar freeze issue
        mProgressBar?.visibility = View.INVISIBLE
        mProgressBar = null

        mRetryButton?.setOnClickListener(null)
        mRetryButton = null
    }

    override fun getLayout(): Int = R.layout.row_user_loading

    fun showRetryButton() {
        mProgressBar?.visibility = View.INVISIBLE
        mRetryButton?.visibility = View.VISIBLE
    }

    fun hideRetryButton() {
        mProgressBar?.visibility = View.VISIBLE
        mRetryButton?.visibility = View.INVISIBLE
    }

    private val mRetryListener = View.OnClickListener {
        hideRetryButton()

        // invoke callback immediately once the progressbar shows
        listener?.invoke(this)
    }
}