package com.technishaun.testroomapp.repository.queuer

import android.content.Context
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

class NetworkRequestQueuer(
    val appContext: Context
): QueuerImpl {

    private val mQueuedTasks = mutableListOf<Observable<Any>>()
    private val mOngoingTasks = mutableListOf<Observable<Any>>()

    override val schedulerForTaskExecution: Scheduler
        get() = Schedulers.single()

    override fun enqueue(request: Request) {
        // set queuer scheduler on request for execution
        request.schedulerAssignedByQueuer = schedulerForTaskExecution
    }
}