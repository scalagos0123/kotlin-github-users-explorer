package com.technishaun.testroomapp.repository.queuer

import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

interface QueuerImpl {
    val schedulerForTaskExecution: Scheduler

    fun enqueue(request: Request)
}