package com.technishaun.testroomapp.repository.local

import com.technishaun.testroomapp.objects.db.User

interface LocalRepositoryImpl {
    fun getUsers(): List<User>
    fun findUser(userOrNoteToSearch: String): User
    fun findUser(id: Int): User
    fun updateUser(user: User)
    fun insertUsers(vararg users: User)
    fun getLastInsertedUserId(): Int
}