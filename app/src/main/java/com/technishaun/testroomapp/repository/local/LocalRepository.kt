package com.technishaun.testroomapp.repository.local

import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.objects.dbinstance.AppDbInstance

class LocalRepository(
        private val appDbInstance: AppDbInstance
): LocalRepositoryImpl {
    override fun getUsers(): List<User> {
        val userDb = appDbInstance.userDao()
        return userDb.getAll()
    }

    override fun findUser(userOrNoteToSearch: String): User {
        val userDb = appDbInstance.userDao()
        return userDb.getUserByName(userOrNoteToSearch)
    }

    override fun updateUser(user: User) {
        val userDb = appDbInstance.userDao()
        userDb.updateUser(user)
    }

    override fun insertUsers(vararg users: User) {
        val userDb = appDbInstance.userDao()
        userDb.insertUsers(*users)
    }

    override fun findUser(id: Int): User {
        val userDb = appDbInstance.userDao()
        return userDb.getUserById(id)
    }

    override fun getLastInsertedUserId(): Int {
        val userDb = appDbInstance.userDao()
        return userDb.getLastInsertedUserId()
    }
}