package com.technishaun.testroomapp.repository.remote

import com.technishaun.testroomapp.objects.response.ResponseGithubUser
import com.technishaun.testroomapp.objects.response.ResponseGithubUsers
import io.reactivex.rxjava3.core.Observable

interface RemoteRepositoryImpl {
    suspend fun getUsers(lastUserId: Int = 0): ResponseGithubUsers
    suspend fun getUserInfo(username: String): ResponseGithubUser
}