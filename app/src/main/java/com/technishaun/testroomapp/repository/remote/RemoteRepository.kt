package com.technishaun.testroomapp.repository.remote

import com.technishaun.testroomapp.objects.response.ResponseGithubUser
import com.technishaun.testroomapp.objects.response.ResponseGithubUsers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class RemoteRepository(
    private val apiService: ApiService
): RemoteRepositoryImpl {
    override suspend fun getUsers(lastUserId: Int): ResponseGithubUsers =
        apiService.getUsers(lastUserId)

    override suspend fun getUserInfo(username: String): ResponseGithubUser =
        apiService.getUserInfo(username)
}