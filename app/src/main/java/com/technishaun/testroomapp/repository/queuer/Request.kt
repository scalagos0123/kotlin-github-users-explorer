package com.technishaun.testroomapp.repository.queuer

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler

class Request(
    val task: Observable<Any>,
    val subscribeSchedulerForReturns: Scheduler
) {

    var schedulerAssignedByQueuer: Scheduler? = null

    var requestState: Status = Status.QUEUED
        private set

    enum class Status { QUEUED, RUNNING, WAITING_FOR_NETWORK }
}