package com.technishaun.testroomapp.repository.remote

import com.technishaun.testroomapp.objects.response.ResponseGithubUser
import com.technishaun.testroomapp.objects.response.ResponseGithubUsers
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("users")
    suspend fun getUsers(@Query("since") lastId: Int): ResponseGithubUsers

    @GET("users/{username}")
    suspend fun getUserInfo(@Path("username") username: String): ResponseGithubUser
}