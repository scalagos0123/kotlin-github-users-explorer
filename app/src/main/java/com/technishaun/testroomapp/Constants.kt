package com.technishaun.testroomapp

object Constants {
    const val REQUEST_CODE_USER_PROFILE = 1000
    const val BUNDLE_KEY_USER_ID = "user_id"
    const val BUNDLE_KEY_USER_NOTE = "user_note"
    const val USERLIST_INTENT_KEY_BUNDLE = "selected_user"
    const val PROFILE_INTENT_KEY_BUNDLE = "saved_user_data"
}