package com.technishaun.testroomapp

import android.app.Application
import androidx.room.Room
import com.technishaun.testroomapp.objects.dbinstance.AppDbInstance
import com.technishaun.testroomapp.repository.local.LocalRepository
import com.technishaun.testroomapp.repository.local.LocalRepositoryImpl
import com.technishaun.testroomapp.repository.remote.ApiService
import com.technishaun.testroomapp.repository.remote.RemoteRepository
import com.technishaun.testroomapp.repository.remote.RemoteRepositoryImpl
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import timber.log.Timber

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        initAppDatabase()
        initNetwork()
    }

    private fun initAppDatabase() {
        appDbInstance = Room.databaseBuilder(
                this,
                AppDbInstance::class.java,
                DB_NAME
        ).build()

        localRepositoryInstance = LocalRepository(appDbInstance)
    }

    private fun initNetwork() {
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1

        val builder = OkHttpClient.Builder()
            .dispatcher(dispatcher)
            .retryOnConnectionFailure(false)

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        retrofitInstance = Retrofit.Builder()
                .baseUrl(BuildConfig.GITHUB_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build()

        apiService = retrofitInstance.create()

        // starting remote repository
        remoteRepositoryInstance = RemoteRepository(apiService)
    }

    companion object {
        private const val DB_NAME = "user_db"

        lateinit var appDbInstance: AppDbInstance
            private set

        lateinit var retrofitInstance: Retrofit
            private set

        lateinit var apiService: ApiService
            private set

        lateinit var remoteRepositoryInstance: RemoteRepositoryImpl
            private set

        lateinit var localRepositoryInstance: LocalRepositoryImpl
            private set
    }
}