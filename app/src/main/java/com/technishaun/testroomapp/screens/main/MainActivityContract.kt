package com.technishaun.testroomapp.screens.main

import com.technishaun.testroomapp.objects.base.BasePresenter
import com.technishaun.testroomapp.objects.db.User

interface MainActivityContract {
    interface View {
        fun displayUsers(users: List<User>, searchMode: Boolean = false, forceRefresh: Boolean = false)
        fun toggleProgressBar(show: Boolean)
        fun toggleNoInternetConnection(show: Boolean)
        fun showRetryButtonOnProgressItem()
        fun hideRetryButtonOnProgressItem()

        fun showToastMessage(message: String)
    }

    interface Presenter: BasePresenter<View> {
        fun getUsers(lastUserId: Int = -1, fromPagination: Boolean = false)
        fun updateUserNote(userId: Int, userNote: String?)
        suspend fun searchUser(userOrNote: String)

        fun onNetworkAvailable()
        fun onNetworkLost()

        fun onNavigate()
    }
}