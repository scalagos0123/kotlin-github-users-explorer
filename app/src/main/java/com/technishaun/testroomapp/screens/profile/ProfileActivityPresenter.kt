package com.technishaun.testroomapp.screens.profile

import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.repository.local.LocalRepositoryImpl
import com.technishaun.testroomapp.repository.remote.RemoteRepositoryImpl
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.ref.WeakReference
import kotlin.Exception

class ProfileActivityPresenter(
    private val remoteRepository: RemoteRepositoryImpl,
    private val localRepository: LocalRepositoryImpl,
    private val parentJob: Job,
    private val subscribeScheduler: CoroutineDispatcher = Dispatchers.IO,
    private val observeScheduler: CoroutineDispatcher = Dispatchers.Main
): ProfileActivityContract.Presenter {

    private lateinit var mView: WeakReference<ProfileActivityContract.View>
    private val mWorkerScope = CoroutineScope(subscribeScheduler + parentJob)
    private val mUiScope = CoroutineScope(observeScheduler + parentJob)

    private lateinit var mUser: User
    private var mUserIncompleteInfo = false
    private var mOngoingApiRequest = false

    override fun attachView(view: ProfileActivityContract.View) {
        mView = WeakReference(view)
    }

    override fun getUser(userId: Int) {
        if (userId == -1) return

        mWorkerScope.launch {
            try {
                // get user from db
                mUser = localRepository.findUser(userId)

                // check user data if it's complete
                if (mUser.name != null
                    && mUser.followers > -1
                    && mUser.following > -1
                ) {
                    mUserIncompleteInfo = false

                    // load data immediately
                    mUiScope.launch {
                        mView.get()?.let { profileView ->
                            profileView.hideLoadingScreen()
                            profileView.showUserDetails(mUser)
                            mUser.note?.let { profileView.showUserNote(it) }
                        }
                    }
                } else {
                    mUserIncompleteInfo = true
                }
            } catch (e: Exception) {
                mUserIncompleteInfo = true
            }

            try {
                fetchProfileFromApi()
                mUserIncompleteInfo = false
                mUiScope.launch { mView.get()?.let { profileView ->
                    profileView.hideLoadingScreen()
                    profileView.showUserDetails(mUser)
                    mUser.note?.let { profileView.showUserNote(it) }
                }}
            } catch (e: Exception) {
                mOngoingApiRequest = false
                if (mUserIncompleteInfo) {
                    mUiScope.launch {
                        mView.get()?.apply {
                            showLoadingScreen()
                            showRetryButton()
                        }
                    }
                }
            }
        }
    }

    override fun saveNote(note: String) {
        mWorkerScope.launch {
            mUser.note = if (note.isEmpty()) null else note

            try {
                localRepository.updateUser(mUser)

                // marking user as saved for later when going back to users list
                mUiScope.launch { mView.get()?.let {
                    it.markNoteAsSaved(mUser)
                    it.showToastMessage("Note successfully saved")
                }}
            } catch (e: Exception) {
                // show error
                Timber.e(e)
                mUiScope.launch { mView.get()?.showToastMessage("Something unexpected happened. Please try again later.") }
            }
        }
    }

    override fun onNetworkAvailable() {
        mWorkerScope.launch {
            if (!mOngoingApiRequest) {
                if (mUserIncompleteInfo) {
                    mUiScope.launch {
                        mView.get()?.apply {
                            showLoadingScreen()
                            hideRetryButton()
                        }
                    }

                    try {
                        fetchProfileFromApi()
                        mUserIncompleteInfo = false
                        mUiScope.launch { mView.get()?.let { profileView ->
                            profileView.hideLoadingScreen()
                            profileView.showUserDetails(mUser)
                            mUser.note?.let { profileView.showUserNote(it) }
                        }}
                    } catch (e: Exception) {
                        mOngoingApiRequest = false
                        if (mUserIncompleteInfo) {
                            mUiScope.launch {
                                mView.get()?.apply {
                                    showLoadingScreen()
                                    showRetryButton()
                                }
                            }
                        }
                    }
                } else {
                    mUiScope.launch {
                        mView.get()?.hideNoInternet()
                    }
                }
            }
        }
    }

    override fun onNetworkLost() {
        mWorkerScope.launch {
            if (mUserIncompleteInfo) {
                mUiScope.launch { mView.get()?.apply {
                    showLoadingScreen()
                    showRetryButton()
                }}
            } else {
                mUiScope.launch {
                    mView.get()?.showNoInternet()
                }
            }
        }
    }

    override fun onRetry() {
        mWorkerScope.launch {
            try {
                fetchProfileFromApi()
                mUserIncompleteInfo = false
                mUiScope.launch { mView.get()?.let { profileView ->
                    profileView.hideLoadingScreen()
                    profileView.showUserDetails(mUser)
                    mUser.note?.let { profileView.showUserNote(it) }
                }}
            } catch (e: Exception) {
                mOngoingApiRequest = false
                if (mUserIncompleteInfo) {
                    mUiScope.launch {
                        mView.get()?.apply {
                            showLoadingScreen()
                            showRetryButton()
                        }
                    }
                }
            }
        }
    }

    private suspend fun fetchProfileFromApi() {
        mOngoingApiRequest = true

        // call remote api to fetch profile data
        val remoteData = remoteRepository.getUserInfo(mUser.username)

        // update user details
        mUser.imageUrl = remoteData.avatarUrl
        mUser.name = remoteData.name.trim()
        mUser.blog = remoteData.blog.trim()
        mUser.companyName = remoteData.company
        mUser.followers = remoteData.followers
        mUser.following = remoteData.following

        localRepository.updateUser(mUser)

        mOngoingApiRequest = false
    }
}