package com.technishaun.testroomapp.screens.main

import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.repository.local.LocalRepositoryImpl
import com.technishaun.testroomapp.repository.remote.RemoteRepositoryImpl
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*

class MainActivityPresenter(
        private val remoteRepository: RemoteRepositoryImpl,
        private val localRepository: LocalRepositoryImpl,
        private val parentJob: Job,
        private val subscribeScheduler: CoroutineDispatcher = Dispatchers.IO,
        private val observeScheduler: CoroutineDispatcher = Dispatchers.Main
) : MainActivityContract.Presenter {

    private lateinit var mView: WeakReference<MainActivityContract.View>

    private val mUsers = mutableListOf<User>()

    private val mWorkerScope = CoroutineScope(parentJob + subscribeScheduler)
    private val mUiScope = CoroutineScope(parentJob + observeScheduler)

    // marking variable if there's an already ongoing API request
    private var mIsFetchingApi = false

    // Marking variable if device has recently disconnected.
    // Will be reset once network is available
    private var mHasNetworkLostRecently = false

    override fun attachView(view: MainActivityContract.View) {
        mView = WeakReference(view)
    }

    override fun getUsers(lastUserId: Int, fromPagination: Boolean) {
        // launch scope for getting local users and network fetching
        mWorkerScope.launch(parentJob) {
            var actualLastUserId = lastUserId

            if (lastUserId == -1) {
                // wait this first to be finished before calling remote so that remote fetch will properly call for `since` parameter
                try {
                    Timber.e("running worker 1")
                    val localUsers = localRepository.getUsers()
                    actualLastUserId = if (localUsers.isEmpty()) 0 else localUsers.last().id

                    // now add users from the result of filteredUsers (ensured that these values are unique)
                    mUsers.addAll(localUsers)

                    mUiScope.launch(parentJob) {
                        mView.get()?.displayUsers(localUsers, forceRefresh = true)
                    }.join()
                } catch (e: Exception) {
                    showError(e)
                }
            }

            if (!mIsFetchingApi) {
                try {
                    // now fetch API requests after showing local database users
                    if (!fromPagination) mUiScope.launch { mView.get()?.toggleProgressBar(true) }

                    mIsFetchingApi = true
                    val fetchedUsers = performApiRequestToGitHub(actualLastUserId)
                    mUsers.addAll(fetchedUsers)

                    // show only fetched users
                    mUiScope.launch(parentJob) {
                        mView.get()?.let {
                            it.displayUsers(fetchedUsers)
                            if (!fromPagination) it.toggleProgressBar(false)
                        }
                    }
                    mIsFetchingApi = false
                } catch (e: Exception) {
                    mIsFetchingApi = false
                    mUiScope.launch(parentJob) {
                        mView.get()?.apply {
                            if (!fromPagination) toggleProgressBar(false)
                        }
                    }
                    showError(e)
                }
            }
        }
    }

    override fun updateUserNote(userId: Int, userNote: String?) {
        val updatedUserIndex = mUsers.indexOfFirst { it.id == userId }

        // update info
        mUsers[updatedUserIndex].note = userNote
    }

    override suspend fun searchUser(userOrNote: String) {
        mWorkerScope.launch {
            if (userOrNote.isEmpty()) {
                // since we're only dealing with existing users, give an empty list instead.
                mUiScope.launch { mView.get()?.displayUsers(
                    listOf(),
                    searchMode = false,
                    forceRefresh = true
                )}
            } else {
                // using filter to automatically fetch matched items
                val filteredItems = mUsers.filter { it.username.toLowerCase(Locale.getDefault()).contains(userOrNote.toLowerCase(Locale.getDefault()))
                        || (!it.note.isNullOrEmpty() && it.note!!.toLowerCase(Locale.getDefault()).contains(userOrNote.toLowerCase(Locale.getDefault())))}

                mUiScope.launch { mView.get()?.displayUsers(filteredItems, searchMode = true) }
            }
        }.join()
    }

    override fun onNetworkAvailable() {
        mView.get()?.toggleNoInternetConnection(false)

        // launch scope for network fetching
        mWorkerScope.launch(parentJob) {

            // making sure that:
            // 1. no one is currently fetching API in the scope
            // 2. network has recently lost
            if (!mIsFetchingApi && mHasNetworkLostRecently) {
                mUiScope.launch { mView.get()?.hideRetryButtonOnProgressItem() }

                // fetch users database to check for last inserted user id
                val lastUserId = localRepository.getLastInsertedUserId()

                try {
                    mIsFetchingApi = true
                    val fetchedUsers = performApiRequestToGitHub(lastUserId)
                    mUsers.addAll(fetchedUsers)

                    mUiScope.launch(parentJob) {
                        mView.get()?.let {
                            it.displayUsers(mUsers.toList())
                            it.toggleProgressBar(false)
                        }
                    }

                    mIsFetchingApi = false
                    mHasNetworkLostRecently = false
                } catch (e: Exception) {
                    mIsFetchingApi = false
                    mHasNetworkLostRecently = false
                    mView.get()?.toggleProgressBar(false)

                    showError(e)
                }
            }
        }
    }

    override fun onNetworkLost() {
        mView.get()?.toggleNoInternetConnection(true)

        // making sure that updating of this variable is synchronized on worker scope
        mWorkerScope.launch { mHasNetworkLostRecently = true }
        Timber.e("Network lost")
    }

    override fun onNavigate() {
        // cancelling all pending jobs
        mView.get()?.toggleProgressBar(false)
        mIsFetchingApi = false
    }

    private suspend fun performApiRequestToGitHub(lastUserId: Int): List<User> {
        Timber.e("running worker 2")
        val remoteUsers = remoteRepository.getUsers(lastUserId)

        // map remote users to local counterpart
        val mappedRemoteUsers = remoteUsers.map {
            User(
                    username = it.login,
                    imageUrl = it.avatarUrl
            ).apply {
                id = it.id
            }
        }

        Timber.e("worker 2 complete")

        // filter users who are already loaded in list
        val filteredUsers = mappedRemoteUsers.filter { mUsers.find { existingUser -> existingUser.id == it.id } == null }

        // save new remote users to database
        localRepository.insertUsers(*filteredUsers.toTypedArray())
        return filteredUsers
    }

    private fun showError(throwable: Throwable) {
        mUiScope.launch(parentJob) {
            mView.get()?.apply {
                showRetryButtonOnProgressItem()
                throwable.message?.let { showToastMessage(it) }
            }

            Timber.e(throwable)
        }
    }
}