package com.technishaun.testroomapp.screens.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputEditText
import com.technishaun.testroomapp.App
import com.technishaun.testroomapp.Constants
import com.technishaun.testroomapp.R
import com.technishaun.testroomapp.objects.db.User
import kotlinx.coroutines.Job

class ProfileActivity : AppCompatActivity(), ProfileActivityContract.View {

    private lateinit var mPresenter: ProfileActivityContract.Presenter
    private val mJob = Job()
    private var mSavedDetailsAfterSave: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val toolbar = findViewById<MaterialToolbar>(R.id.tb_profile)
        setSupportActionBar(toolbar)

        // starting presenter
        mPresenter = ProfileActivityPresenter(
            remoteRepository = App.remoteRepositoryInstance,
            localRepository = App.localRepositoryInstance,
            parentJob = mJob
        ).apply {
            attachView(this@ProfileActivity)
        }

        initViews(savedInstanceState)
    }

    override fun showNoInternet() {
        findViewById<TextView>(R.id.txtv_no_internet).visibility = View.VISIBLE
    }

    override fun hideNoInternet() {
        findViewById<TextView>(R.id.txtv_no_internet).visibility = View.INVISIBLE
    }

    override fun onResume() {
        registerToNetworkState()
        super.onResume()
    }

    override fun onPause() {
        unregisterToNetworkState()
        super.onPause()
    }

    private fun registerToNetworkState() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            val networkRequest = NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build()

            connectivityManager.registerNetworkCallback(networkRequest, mNetworkCallback)
        }
    }

    private fun unregisterToNetworkState() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.unregisterNetworkCallback(mNetworkCallback)
    }

    override fun showRetryButton() {
        findViewById<Button>(R.id.btn_retry)?.apply {
            visibility = View.VISIBLE
        }

        findViewById<ProgressBar>(R.id.prg_loading).visibility = View.INVISIBLE
    }

    override fun hideRetryButton() {
        findViewById<Button>(R.id.btn_retry)?.apply {
            visibility = View.INVISIBLE
        }

        findViewById<ProgressBar>(R.id.prg_loading).visibility = View.VISIBLE
    }

    override fun showLoadingScreen() {
        findViewById<ConstraintLayout>(R.id.cl_retry_header).visibility = View.VISIBLE
    }

    override fun hideLoadingScreen() {
        findViewById<ConstraintLayout>(R.id.cl_retry_header).visibility = View.GONE
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun initViews(savedInstanceState: Bundle?) {
        intent.getBundleExtra(Constants.USERLIST_INTENT_KEY_BUNDLE)?.let {
            // set everything only when user ID exists on intent
            val userId = it.getInt(Constants.BUNDLE_KEY_USER_ID, -1)

            findViewById<ImageButton>(R.id.btn_back).setOnClickListener {
                onBackPressed()
            }

            findViewById<Button>(R.id.btn_save).setOnClickListener {
                mPresenter.saveNote(findViewById<TextInputEditText>(R.id.etxt_note).text.toString())
            }

            // basically re-show progress bar after pressing retry
            findViewById<Button>(R.id.btn_retry)?.setOnClickListener { button ->
                findViewById<ProgressBar>(R.id.prg_loading).visibility = View.VISIBLE
                button.visibility = View.INVISIBLE
                mPresenter.onRetry()
            }

            mPresenter.getUser(userId)
        }
    }

    override fun showUserDetails(user: User) {
        findViewById<TextView>(R.id.txtv_followers).text = resources.getString(R.string.profile_number_template, "Followers", user.followers)
        findViewById<TextView>(R.id.txtv_following).text = resources.getString(R.string.profile_number_template, "Following", user.following)
        findViewById<TextView>(R.id.txtv_name).text = resources.getString(R.string.profile_text_template, "Name", user.name ?: "N/A")
        findViewById<TextView>(R.id.txtv_blog).text = resources.getString(R.string.profile_text_template, "Blog", user.blog ?: "N/A")
        findViewById<TextView>(R.id.txtv_company).text = resources.getString(R.string.profile_text_template, "Company", user.companyName ?: "N/A")

        // it's possible that there are users whose names aren't set,
        findViewById<TextView>(R.id.txtv_toolbar_name).text = user.name

        Glide.with(this)
            .load(user.imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(findViewById(R.id.imgv_profile_user))
    }

    override fun showUserNote(userNote: String) {
        findViewById<TextInputEditText>(R.id.etxt_note).setText(userNote)
    }

    override fun markNoteAsSaved(user: User) {
        mSavedDetailsAfterSave = user
    }

    override fun onBackPressed() {
        if (mSavedDetailsAfterSave != null) {
            val returnIntent = Intent()
            returnIntent.putExtra(
                Constants.PROFILE_INTENT_KEY_BUNDLE,
                Bundle().apply {
                    putInt(Constants.BUNDLE_KEY_USER_ID, mSavedDetailsAfterSave!!.id)
                    putString(Constants.BUNDLE_KEY_USER_NOTE, mSavedDetailsAfterSave!!.note)
                }
            )

            setResult(Activity.RESULT_OK, returnIntent)
        } else {
            setResult(Activity.RESULT_CANCELED)
        }

        finish()
    }

    private val mNetworkCallback = object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            runOnUiThread { mPresenter.onNetworkAvailable() }
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            runOnUiThread { mPresenter.onNetworkLost() }
        }
    }
}