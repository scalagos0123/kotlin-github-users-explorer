package com.technishaun.testroomapp.screens.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.technishaun.testroomapp.App
import com.technishaun.testroomapp.Constants
import com.technishaun.testroomapp.R
import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.objects.listitem.ItemLoading
import com.technishaun.testroomapp.objects.listitem.ItemUserList
import com.technishaun.testroomapp.screens.profile.ProfileActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity(), MainActivityContract.View {

    private lateinit var mPresenter: MainActivityContract.Presenter
    private val mListItems = mutableListOf<Item<GroupieViewHolder>>()
    private val mJob = Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.materialToolbar))

        // starting presenter
        mPresenter = MainActivityPresenter(
            remoteRepository = App.remoteRepositoryInstance,
            localRepository = App.localRepositoryInstance,
            parentJob = mJob
        )

        mPresenter.attachView(this@MainActivity)
        initViews(savedInstanceState)
    }

    override fun toggleProgressBar(show: Boolean) {
        findViewById<ProgressBar>(R.id.prg_loading).visibility = if (show) View.VISIBLE else View.INVISIBLE
    }

    override fun toggleNoInternetConnection(show: Boolean) {
        findViewById<TextView>(R.id.txtv_no_internet).visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun displayUsers(users: List<User>, searchMode: Boolean, forceRefresh: Boolean) {
        if (searchMode) {
            val searchResults = convertAllUsersToItemList(users, true)
            refreshListItems(searchResults, true)
        } else {
            mListItems.lastOrNull()?.let {
                if (it is ItemLoading) {
                    mListItems.removeAt(mListItems.size - 1)
                }
            }

            mListItems.addAll(convertAllUsersToItemList(users))

            if (mListItems.isNotEmpty()) {
                val lastItem = mListItems.last() as ItemUserList

                // add pagination item
                mListItems.add(
                    ItemLoading(
                        lastItem.user.id
                    ).apply {
                        listener = ::onLoadingShown
                    })

                // update list items
                refreshListItems(mListItems, forceRefresh)
            }
        }
    }

    override fun showRetryButtonOnProgressItem() {
        if (mListItems.isNotEmpty()) {
            val progressItemIndex =  mListItems.indexOfFirst { it is ItemLoading }
            (mListItems[progressItemIndex] as ItemLoading).showRetryButton()
        } else {
            // only show this when there are no list items available
            // forcing loading item id to 0
            mListItems.add(ItemLoading(0, initialInvokeOnBind = true).apply {
                listener = ::onLoadingShown
            })

            refreshListItems(mListItems, true)
        }
    }

    override fun hideRetryButtonOnProgressItem() {
        if (mListItems.isNotEmpty()) {
            val progressItemIndex = mListItems.indexOfFirst { it is ItemLoading }
            (mListItems[progressItemIndex] as ItemLoading).hideRetryButton()
        }
    }

    override fun onResume() {
        super.onResume()

        // putting here in onResume so when the activity resumes,
        // TODO network callback initialization on app start
        registerToNetworkState()
    }

    override fun onPause() {
        super.onPause()
        unregisterToNetworkState()
    }

    override fun onDestroy() {
        super.onDestroy()

        // force cancelling all pending jobs when activity is destroyed
        mJob.cancel()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Constants.REQUEST_CODE_USER_PROFILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    // fetch data from profile
                    val bundle = data!!.getBundleExtra(Constants.PROFILE_INTENT_KEY_BUNDLE)!!
                    val userId = bundle[Constants.BUNDLE_KEY_USER_ID] as Int
                    val userNote = bundle[Constants.BUNDLE_KEY_USER_NOTE] as? String

                    mPresenter.updateUserNote(userId, userNote)

                    updateUserOnUi(userId, userNote)
                }
            }
        }
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun registerToNetworkState() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, mNetworkCallback)
    }

    private fun unregisterToNetworkState() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.unregisterNetworkCallback(mNetworkCallback)
    }

    private fun refreshListItems(listItems: List<Item<GroupieViewHolder>>, forceRefresh: Boolean = false) {
        with(findViewById<RecyclerView>(R.id.recv_user_list)) {
            (adapter as GroupAdapter<*>).apply {
                if (!forceRefresh)
                    updateAsync(listItems)
                else
                    replaceAll(listItems)
            }
        }
    }

    private fun updateUserOnUi(userId: Int, userNote: String?) {
        val updatedUserIndex = mListItems.indexOfFirst { (it as ItemUserList).user.id == userId }
        (mListItems[updatedUserIndex] as ItemUserList).user.note = userNote

        refreshListItems(mListItems, true)
    }

    private fun onLoadingShown(item: ItemLoading) {
        mPresenter.getUsers(item.nextUserId, fromPagination = true)
    }

    private fun onUserSelected(item: ItemUserList) {
        // trigger onNavigate to check for pending jobs
        mPresenter.onNavigate()

        val intent = Intent(this, ProfileActivity::class.java)
        intent.putExtra(
            Constants.USERLIST_INTENT_KEY_BUNDLE,
            Bundle().apply {
                putInt(Constants.BUNDLE_KEY_USER_ID, item.user.id)
            }
        )

        startActivityForResult(intent, Constants.REQUEST_CODE_USER_PROFILE)
    }

    private fun convertAllUsersToItemList(users: List<User>, searchMode: Boolean = false): List<ItemUserList> {
        if (!searchMode) {
            // map newly added users
            return users.mapIndexed { newUsersIndex, it ->
                // subtracting to 2 because last item is a loader
                val lastIndexInList = if (mListItems.size < 3) 0 else mListItems.size - 2

                // (last index in UI item list + new users index + 1) will be the marker for inverted avatars
                ItemUserList(it, (lastIndexInList + newUsersIndex + 1) % 4 == 0).apply {
                    listener = { user ->
                        onUserSelected(user)
                    }
                }
            }
        } else {
            return users.mapIndexed { newUsersIndex, it ->
                // this time, checking for every 4th image is dependent on searchMode-based list
                ItemUserList(it, (newUsersIndex + 1) % 4 == 0).apply {
                    listener = ::onUserSelected
                }
            }
        }
    }

    private fun initViews(savedInstanceState: Bundle?) {
        initList()

        // listen to search inputs
        findViewById<TextInputEditText>(R.id.etxt_search).addTextChangedListener(mTextWatcher)
        mPresenter.getUsers()
    }

    private fun initList() {
        with(findViewById<RecyclerView>(R.id.recv_user_list)) {
            adapter = GroupAdapter<GroupieViewHolder>()
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        }
    }

    private val mTextWatcher = object: TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            runBlocking { mPresenter.searchUser(s.toString()) }
        }

        override fun afterTextChanged(s: Editable?) {

        }
    }

    private val mNetworkCallback = object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            runOnUiThread { mPresenter.onNetworkAvailable() }
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            runOnUiThread { mPresenter.onNetworkLost() }
        }
    }
}