package com.technishaun.testroomapp.screens.profile

import com.technishaun.testroomapp.objects.base.BasePresenter
import com.technishaun.testroomapp.objects.db.User
import com.technishaun.testroomapp.objects.response.ResponseGithubUser

interface ProfileActivityContract {
    interface View {
        fun showUserDetails(user: User)
        fun showUserNote(userNote: String)
        fun markNoteAsSaved(user: User)

        fun showToastMessage(message: String)

        fun showRetryButton()
        fun hideRetryButton()
        fun showLoadingScreen()
        fun hideLoadingScreen()

        fun showNoInternet()
        fun hideNoInternet()
    }

    interface Presenter: BasePresenter<View> {
        fun getUser(userId: Int)
        fun saveNote(note: String)

        fun onRetry()
        fun onNetworkAvailable()
        fun onNetworkLost()
    }
}